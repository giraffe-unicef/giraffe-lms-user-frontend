<!---
Please use the below template to report Issues!

Before opening a new issue, make sure to search for keywords in the issues
filtered by the "regression" or "bug" label:

- https://gitlab.com/giraffe-unicef/giraffe-lms-api/-/issues

and verify the issue you're about to submit has not already been submitted
--->

### Summary

<!-- Summarize the bug encountered concisely -->

### Steps to reproduce

<!-- Describe how the issue can be reproduced - this is very important! Please use an ordered list -->

### What is the current bug behavior?

<!-- Describe what actually happens -->

### What is the expected/ correct behavior?

<!-- Describe what you expect to see instead -->

### Relevant logs and/or screenshots

<!-- Paste any relevant logs - please use code blocks (```) to format console output, logs, and code as it's challenging to diagnose -->

### Labels/ tags

<!--  If possible assign a relevant label/tag -->
